<?php

if( ! function_exists('msg'))
{
    function msg($message = null)
    {
        $notifier = app('msg');

        if ( ! is_null($message))
        {
            return $notifier->info($message);
        }

        return $notifier;
    }
}