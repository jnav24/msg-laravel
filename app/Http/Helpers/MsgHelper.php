<?php

namespace App\Http\Helpers;

class MsgHelper
{
    public function __construct()
    {

    }
    public function info($msg)
    {
        $this->message($msg, 'info');

        return $this;
    }

    public function success($msg)
    {
        $this->message($msg, 'success');

        return $this;
    }

    public function error($msg)
    {
        $this->message($msg, 'error');

        return $this;
    }

    public function warn($msg)
    {
        $this->message($msg, 'warning');

        return $this;
    }

    public function retain()
    {
        \Session::flash('msg_notify.close', true);

        return $this;
    }

    public function message($msg, $class)
    {
        \Session::flash('msg_notify.message', $msg);
        \Session::flash('msg_notify.class', $class);

        return $this;
    }
}