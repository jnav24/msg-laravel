<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MsgServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindShared('msg', function() {
            return $this->app->make('App\Http\Helpers\MsgHelper');
        });
    }
}