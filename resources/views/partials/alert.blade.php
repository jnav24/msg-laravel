@if(Session::has('msg_notify.message'))
    <div class="alert alert-{{ Session::get('msg_notify.class') }} {{ Session::has('msg_notify.close') ? 'alert-retain' : '' }}">
        @if(Session::has('msg_notify.close'))
            <div class="close">&times;</div>
        @endif

        {{ Session::get('msg_notify.message') }}
    </div>

    <script>
        $(function() {
        	$('.alert .close').on('click', function() {
		        $(this).closest('.alert').fadeOut();
		    });

		    if(!$('.alert-retain').is('*'))
		    {
		        setTimeout(function() {
		            $('.alert').fadeOut();
		        }, 3000);
		    }
        });
    </script>
@endif