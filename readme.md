# Msg

A session flash messaging tool for laravel.

In the config/app.php, add this under the providers array

```
App\Providers\MsgServiceProvider::class,
```

In the config/app.php, add this under the aliases array

```
'Msg'      => App\Http\Facades\MsgFacade::class,
```

In the controller, an example of the msg to send to the session

```
msg()->success('Success message here');
```

You will have to include the alert.blade.php file and import the alerts sass file.